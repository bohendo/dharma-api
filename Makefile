# Variable

project=dcm
proxy_image="$(project)_proxy"
core_image="$(project)_core"

VPATH=src:ops:build

webpack=node_modules/.bin/webpack

src=$(shell find src -type f -name "*.js")
ops=$(shell find ops -type f)
#contracts=$(shell find contracts -type f -name "*.json")

$(shell mkdir -p build)


# Rules

all: core-image proxy-image

clean:
	rm -rf build/*

push: core-image proxy-image
	docker push bohendo/$(proxy_image)
	docker push bohendo/$(core_image)

proxy-image: $(ops)
	docker build -f ops/proxy.Dockerfile -t $(proxy_image) .
	touch build/proxy-image

core-image: bundle.js core.Dockerfile
	docker build -f ops/core.Dockerfile -t $(core_image) .
	touch build/core-image

bundle.js: node-modules webpack.js $(src)
	$(webpack) --config ./ops/webpack.js

node-modules: package.json
	yarn install
	touch build/node-modules

import express from 'express'
import createOrder from './create_order'
import db from './db'
import fillOrder from './fill'
import submitDebtOrder from './submit_debt_order'

const api = express.Router()

api.get('/get', (request, response) => {
    response.json(db.getDebtOrders())
})

api.post('/create', (request, response) => {
    return createOrder(request.body).then(order => {
        console.log(`\n\nResponding to Create ${JSON.stringify(order)}`)
        return response.json(order)
    })
})

api.post('/submit', (request, response) => {
    console.log(`\n\nSubmiting debt request ${JSON.stringify(request.body)}`)
    response.send(submitDebtOrder(request.body))
})

api.post('/cancel', (request, response) => {
    response.send('Cancelling Loan request')
})

api.post('/fill', (request, response) => {
    fillOrder(request.body).then(txHash => {
        console.log(`Debt order filled! txHash: ${txHash}`)
        response.json({
            message: 'Successfully filled order',
            txHash
        })
    })
})

export default api

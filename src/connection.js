import Dharma from '@dharmaprotocol/dharma.js'
import Web3 from 'web3'

import ethutil from 'ethereumjs-util'

const ethProvider = process.env.ETH_PROVIDER

const web3 = new Web3(new Web3.providers.HttpProvider(ethProvider))

const dharma = new Dharma(ethProvider)
dharma.web3 = web3

/*
delete web3.sendTransaction
web3.sendTransation = () => { console.log('sending transaction! (not really)') }
*/

dharma.web3.eth.getBlock('latest').then(block => {
    console.log(`Eth provider synced to block ${block.number}`)
}).catch(error => {
    console.error(`Couldn't connect to eth provider`)
    process.exit(1)
})

global.Dharma = Dharma
global.dharma = dharma
global.web3 = dharma.web3

export { dharma, web3 }

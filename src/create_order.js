import BigNumber from 'bignumber.js'
import web3Utils from 'web3-utils'
import { dharma } from './connection'
import params from './params'

var debtKernel
var termsContract
const loadContracts = async () => {
    if (!debtKernel) {
        console.log(`Fetching Debt Kernel...`)
        debtKernel = await dharma.contracts.loadDebtKernelAsync()
        console.log(`Debt Kernel Loaded!`)
    }
    if (!termsContract) {
        console.log(`Fetching Terms Contract...`)
        termsContract = await dharma.contracts.loadCollateralizedSimpleInterestTermsContract()
        console.log(`Terms Contract Loaded!`)
    }
    return { debtKernel, termsContract }
}

var tokenIndicies = {}
const getTokenIndicies = async (principal, collateral) => {
    if (!tokenIndicies[principal]) {
        console.log(`Fetching principal token indicies...`)
        tokenIndicies[principal] = await dharma.contracts.getTokenIndexBySymbolAsync(principal)
        console.log(`Principal Token Indicies Loaded!`)
    }
    if (!tokenIndicies[collateral]) {
        console.log(`Fetching collateral token indicies...`)
        tokenIndicies[collateral] = await dharma.contracts.getTokenIndexBySymbolAsync(collateral)
        console.log(`Collateral Token Indicies Loaded!`)
    }
    return {
        principalIndex: tokenIndicies[principal],
        collateralIndex: tokenIndicies[collateral],
    }
}

const getSeconds = (duration, unit) => {
   if (unit.substring(0,4) === 'hour')  return new BigNumber(duration).mul(new BigNumber(3600))
   if (unit.substring(0,3) === 'day')  return new BigNumber(duration).mul(new BigNumber(86400))
   if (unit.substring(0,4) === 'week')  return new BigNumber(duration).mul(new BigNumber(604800))
   if (unit.substring(0,5) === 'month')  return new BigNumber(duration).mul(new BigNumber(2628000))
   if (unit.substring(0,4) === 'year')  return new BigNumber(duration).mul(new BigNumber(31536000))
}

const createOrder = async (body) => {
    const {
        principalAmount,
        principalToken,
        collateralAmount,
        collateralToken,
        interestRate,
        termDuration,
        termUnit,
        debtorAddress,
        expiresInDuration,
        expiresInUnit,
    } = body

    const { debtKernel, termsContract } = await loadContracts()

    const { principalIndex, collateralIndex } = await getTokenIndicies(principalToken, collateralToken)

    const siTermContractParams = {
        amortizationUnit: termUnit,
        interestRate: new BigNumber(interestRate),
        principalAmount: new BigNumber(principalAmount),
        principalTokenIndex: principalIndex,
        termLength: new BigNumber(termDuration),
    }

    const collateralizedParams = {
        collateralAmount: new BigNumber(collateralAmount),
        collateralTokenIndex: collateralIndex,
        gracePeriodInDays: new BigNumber('0'),
    }

    const termParams = dharma.adapters.collateralizedSimpleInterestLoan.packParameters(siTermContractParams, collateralizedParams)

    const issuance = {
        version: debtKernel.address,
        debtor: debtorAddress,
        underwriter: null,
        underwriterRiskRating: 0,
        termsContract: termsContract.address,
        termsContractParameters: termParams,
        salt: BigNumber.random(20).times(new BigNumber(10).pow(20)),
    }

    issuance.agreementId = web3Utils.soliditySha3(issuance)

    const loanRequest = {
        agreementId: issuance.agreementId,
        underwriterFee: 0,
        principalAmount: new BigNumber(principalAmount),
        principalTokenIndex: principalIndex,
        debtorFee: 0,
        creditorFee: 0,
        relayer: process.env.ETH_ADDRESS,
        relayerFee: new BigNumber(principalAmount).div(new BigNumber(params.RELAYER_FEE)),
        expirationTimestampInSec: getSeconds(expiresInDuration, expiresInUnit)
    }

    const debtOrder = {
        issuance,
        loanRequest,
        loanRequestHash: web3Utils.soliditySha3(loanRequest),
        terms: {
            amortizationUnit: termUnit,
            interestRate: new BigNumber(interestRate),
            principalAmount: new BigNumber(principalAmount),
            principalToken: principalToken,
            termLength: new BigNumber(termDuration),
            collateralAmount: new BigNumber(collateralAmount),
            collateralToken: collateralToken,
            gracePeriodInDays: new BigNumber('0'),
        }
    }

    return debtOrder
}

loadContracts()
getTokenIndicies('WETH', 'REP')

export default createOrder

import BigNumber from 'bignumber.js'
import web3Utils from 'web3-utils'
import ethutils from 'ethereumjs-util'
import params from './params'

const validate = {}

validate.loanRequest = (debtOrder) => {

    if (debtOrder.loanRequest.relayer != process.env.ETH_ADDRESS ) return false

    if (debtOrder.loanRequest.relayerFee != new BigNumber(debtOrder.terms.principalAmount).div(new BigNumber(params.RELAYER_FEE))) return false

    // verify the hash of data is what we expected
    const expectedHash = web3Utils.soliditySha3(debtOrder.loanRequest)
    if (expectedHash != debtOrder.loanRequestHash)
        return false

    return true
}

validate.signature = (signature, loanRequestHash, signerAddress) => {

    const pubK = ethutils.ecrecover(ethutils.toBuffer(loanRequestHash), signature.v, signature.r, signature.s)

    const ethAddress = ethutils.baToJSON(ethutils.pubToAddress(pubK))

    console.log(`\n\n\n ethAddress = ${JSON.stringify(ethAddress)}, \n signerAddress = ${JSON.stringify(signerAddress)}`)

    if (ethAddress !== signerAddress)
        return false
    return true
}

export default validate

const db = {}
let debtOrders = []

const mockDebtOrder = {
    "issuance":{"version":"0x755e131019e5ab3e213dc269a4020e3e82e06e20","debtor":"0x7ddc60213ad41f978688b4091dc7e03d663d9ca4","underwriter":null,"underwriterRiskRating":0,"termsContract":"0x13763cf3eb3b6813fa800d4935725a0504c8eb8f","termsContractParameters":"0x0400000000000000000000000501e07830006010000000000000000000006400","salt":"16151519248288657642","agreementId":"0xa61135c38cb595846a5cdacdde4739575dd1ea59a7c213fb40ac55e434eb1696"},
    "loanRequest":{"agreementId":"0xa61135c38cb595846a5cdacdde4739575dd1ea59a7c213fb40ac55e434eb1696","underwriterFee":0,"principalAmount":"5","principalTokenIndex":"4","debtorFee":0,"creditorFee":0,"relayer":"0x80e0d8d9917eb48cad307be74a4541cc93cd367a","relayerFee":"0.2","expirationTimestampInSec":"432000"},
    "loanRequestHash":"0xb92dfc3aaba6d474581b0d5f7de4592dbac5ef5fd6b193588f1971db8286c7e2",
    "terms":{"amortizationUnit":"months","interestRate":"12.3","principalAmount":"5","principalToken":"WETH","termLength":"6","collateralAmount":"100","collateralToken":"REP","gracePeriodInDays":"0"},
    "debtorSignature":{"v":"0x1b","r":"0x95700c8f01ea5005fc409b9569b3ceca0923f30950bf1f13577c86d419c074be","s":"0x1a7bd5aa4d923f46b157444d24ddb3fd547d4ebf2dfa4f340383bf9c551088b2"}}

debtOrders.push(mockDebtOrder)

db.getDebtOrders = () => {
    console.log(`DB: fetching debt orders...`)
    return debtOrders
}

db.saveDebtOrder = (debtOrder) => {
    console.log(`DB: saving debt order`)
    if (debtOrder) {
        debtOrders.push(debtOrder)
        return true
    }
    return false
}

db.removeDebtOrder = (order) => {
    const filterByAgreementId = (orderRequest) => {
        if (order.loanRequest.agreementId === orderRequest.loanRequest.agreementId) 
            return false
        return true
    }

    debtOrders = debtOrders.filter(filterByAgreementId)
}

export default db

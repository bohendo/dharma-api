import express from 'express'
import bodyParser from 'body-parser'
import api from './api'
import db from './db'

const router = express.Router()

router.use(bodyParser.json())

router.use((request, response, next) => {
    console.log(`${request.method} Received: ${JSON.stringify({
        path: request.path,
        body: request.body
    })}`)
    next()
})

router.use('/', api)

// Add real endpoints here
router.use('/api', api)
router.use('/', (request, response) => {
    response.status(200).json({ title: 'Open Debt Orders', data: db.getDebtOrders() })
})

// End of pipeline
router.use((request, response) => {
    let message = `Unknown API Endpoint: ${request.path}`
    response.status(404).json({
        body: message
    })
})

// error handler
router.use((error, request, response) => {
   response.json(error)
})

const port = 8000
express().use(router).listen(port, () => { console.log(`listening on port ${port}`) })
